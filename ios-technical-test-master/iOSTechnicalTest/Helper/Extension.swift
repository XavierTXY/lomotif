//
//  Extension.swift
//  iOSTechnicalTest
//
//  Created by XavierTanXY on 19/9/19.
//  Copyright © 2019 Lomotif. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import SDWebImage

//A class for storing extension

extension UIImageView {
    
    
    //A method where it loads image from an url
    func loadImageUsingCacheWithUrlString(url: String) {
        let actIndicator = UIActivityIndicatorView()
        actIndicator.hidesWhenStopped = true
        
        
        self.addSubview(actIndicator)
        
        self.image = nil
        actIndicator.startAnimating()
        //Running using main queue
        DispatchQueue.main.async() {
            actIndicator.center = self.center
            //Get image from cache if there is, else download using Alamofire and store in cache
            SDImageCache.shared.queryCacheOperation(forKey: (url as NSString) as String!, done: { (image, data, SDImageCacheType) in
                
                //if there is image in cache
                if( image != nil) {
                    self.image = image
                    actIndicator.stopAnimating()
                    
                } else {
                    
                    //Download using Alamofire
                    Alamofire.request(url).responseData { response in
                        
                        if let imageData = response.result.value {
                            let image = UIImage(data: imageData)
                            self.image = image
                            actIndicator.stopAnimating()
                            //Store in cache
                            SDImageCache.shared.store(image, forKey: (url as NSString) as String!)
                        } else {
                            
                            //If a track has no image, use default image
                            self.image = nil
                            self.backgroundColor = UIColor.gray
                            actIndicator.stopAnimating()
                        }
                    }
                }
            })
            
            
        }
        
    }
}
