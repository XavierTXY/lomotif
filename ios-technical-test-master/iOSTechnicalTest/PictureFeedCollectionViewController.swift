//
//  PictureFeedCollectionViewController.swift
//  iOS Technical Test
//
//  Created by Casey Law on 11/12/18.
//  Copyright © 2018 Lomotif. All rights reserved.
//

import UIKit



class PictureFeedCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    var pixabayDataArray = [PixabayData]()
    var currentPage = 1
    var maxPage = 3
    var url = "https://pixabay.com/api/?key=10961674-bf47eb00b05f514cdd08f6e11&page="
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //Fetch first page of images
        self.fetchUrl(url: "\(url)\(currentPage)")

    }
    
    
    func fetchUrl(url: String){
        
        let urlString = URL(string: url)
        
        if let url = urlString {
       
            let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
                
                if error != nil {
                    print(error.debugDescription)
                } else {
                    
                    if let usableData = data {
                        do {
                            
                            let json = try JSONSerialization.jsonObject(with: usableData, options: []) as! Dictionary<String, AnyObject>
                            
                            if let array = json["hits"] as? [Dictionary<String, AnyObject>] {

                                for data in array {
                                    let t = PixabayData(data: data)

                                    //append all fetched data to array for displaying
                                    self.pixabayDataArray.append(t)

                                }

                                DispatchQueue.main.async() {
                                    //refresh the view
                                    self.collectionView.reloadData()
                                }


                            }
                            
                        } catch let error as NSError {
                            
                            print(error.debugDescription)
                        }
                    }
                }
            }
            
            task.resume()
            
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return pixabayDataArray.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PictureCell", for: indexPath) as! PictureCell
        
        cell.configureCell(data: pixabayDataArray[indexPath.row])
    
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        return CGSize(width: CGFloat((collectionView.frame.size.width / 3) - 3), height: CGFloat((collectionView.frame.size.width / 3)))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 3
    }
    
    //Pagination up to 3 pages when it hits the bottom of pages
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == pixabayDataArray.count - 1 {
            
            if (currentPage + 1) <= maxPage {
                currentPage = currentPage + 1
                self.fetchUrl(url: "\(url)\(currentPage)")
            }
            
        }
    }
    


    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}
