//
//  PixabayData.swift
//  iOSTechnicalTest
//
//  Created by XavierTanXY on 19/9/19.
//  Copyright © 2019 Lomotif. All rights reserved.
//

import Foundation

class PixabayData {
 
    private var _imgUrl: String
    
    var imgUrl: String {
        
        get {
            return _imgUrl
        }
        
        set( newUrl ) {
            _imgUrl = newUrl
        }
        
    }
    
    init(data: Dictionary<String, AnyObject>) {
        
        if let url = data["previewURL"] as? String {
            self._imgUrl = url
            
        } else {
            self._imgUrl = ""
        }
    }
}
