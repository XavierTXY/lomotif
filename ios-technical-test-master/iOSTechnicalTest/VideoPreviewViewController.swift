//
//  VideoPreviewViewController.swift
//  iOS Technical Test
//
//  Created by Casey Law on 11/12/18.
//  Copyright © 2018 Lomotif. All rights reserved.
//

import UIKit
import UIKit
import AVFoundation
import AVKit
import AssetsLibrary

class VideoPreviewViewController: UIViewController {

    @IBOutlet weak var startBtn: UIButton!
    @IBOutlet weak var durationLbl: UILabel!
    @IBOutlet weak var durationProgreeBar: UIProgressView!
    
    @IBOutlet weak var videoView: VideoView!
    
    var MAXTIME: Float!
    var currentTime: Float = 0.0
    let timeInterval: Float = 0.05
    var videoPathURL: NSURL?
    
    var isVidPlaying = false
    var progressTimer: Timer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        durationProgreeBar.setProgress(currentTime, animated: true)
        self.startBtn.layer.cornerRadius = 8

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.videoView.previewVC = self
    }
    
    //download audio from the url provided
    func downloadAudio() {
        if let audioUrl = URL(string: "https://audio-ssl.itunes.apple.com/apple-assets-us-std-000001/AudioPreview122/v4/8a/dd/1f/8add1f4d-142c-1317-250d-ff6370962fb8/mzaf_7601694821840779604.plus.aac.p.m4a") {

            // then lets create your document folder url
            let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!

            // lets create your destination file url
            let destinationUrl = documentsDirectoryURL.appendingPathComponent(audioUrl.lastPathComponent)


            // to check if it exists before downloading it
            if FileManager.default.fileExists(atPath: destinationUrl.path) {
                print("The file already exists at path")
                self.playVidAudioFromURL(audioURL: destinationUrl)

                // if the file doesn't exist
            } else {

                // you can use NSURLSession.sharedSession to download the data asynchronously

                URLSession.shared.downloadTask(with: audioUrl) { location, response, error in
                    guard let location = location, error == nil else { return }
                    do {
                        // after downloading your file you need to move it to your destination url
                        try FileManager.default.moveItem(at: location, to: destinationUrl)
                        print("File moved to documents folder")
                        
                        self.playVidAudioFromURL(audioURL: destinationUrl)
                    } catch {
                        print(error)
                    }
                }.resume()
            }
        }
    }
    
    //start btn tapped
    @IBAction func startTapped(_ sender: Any) {
        
        if isVidPlaying {
            self.vidStop()
        } else {

            if videoPathURL != nil {
                self.vidPlay()
            } else {

                downloadAudio()

            }

        }

    }
    
    //Merge video with audio before playing
    func playVidAudioFromURL(audioURL: URL) {
        if let path = Bundle.main.path(forResource: "Movie", ofType: "mp4") {
   
                let videoUrl = NSURL(fileURLWithPath: path)
                let audioUrl = audioURL as! NSURL
        
                //get merged url
                let url = self.mergeFilesWithUrl(videoUrl: videoUrl, audioUrl: audioUrl)
            
                videoView.configure(url: "\(url)")
                self.videoPathURL = url
                self.vidPlay()

                MAXTIME = videoView.getVidDuration(vidUrl: url)

                let interval = CMTime(seconds: 0.5, preferredTimescale: 2)
            
                //update progress bar
                videoView.player?.addPeriodicTimeObserver(forInterval: interval, queue: DispatchQueue.main, using: { (progressTime) in

                    let sec = CMTimeGetSeconds(progressTime)
                    self.updateBar(currentSec: sec)
                })
            
            
            }
            

    }
    
    /*
     // MARK: - Update UI Functions
     */
    //Update progress bar bae on the duration and current time of the video
    @objc func updateBar(currentSec: Float64) {
        durationProgreeBar.progress = (Float(currentSec)/(MAXTIME))
        
        let secondString = String(format: "%02d", Int(currentSec.truncatingRemainder(dividingBy: 60.0)))
        durationLbl.text = "00:\(secondString)"
        
        //stops here
        if currentTime > MAXTIME {
            currentTime = 0.0
        }
        
    }
    
    /*
     // MARK: - Video functions
     */
    
    //Vid reach the end and reset
    func vidReachEnd() {
        self.startBtn.setTitle("Play", for: .normal)
        isVidPlaying = false
    }
    
    //Plays Vid
    func vidPlay() {
        self.startBtn.setTitle("Stop", for: .normal)
        videoView.play()
        isVidPlaying = true
    }
    
    //Stops Vid
    func vidStop() {
        self.startBtn.setTitle("Play", for: .normal)
        videoView.pause()
        isVidPlaying = false
    }
    
    /*
    // MARK: - Merging function
    */
    
    func mergeFilesWithUrl(videoUrl:NSURL, audioUrl:NSURL) -> NSURL
    {
        let mixComposition : AVMutableComposition = AVMutableComposition()
        var mutableCompositionVideoTrack : [AVMutableCompositionTrack] = []
        var mutableCompositionAudioTrack : [AVMutableCompositionTrack] = []
        let totalVideoCompositionInstruction : AVMutableVideoCompositionInstruction = AVMutableVideoCompositionInstruction()
        
        
        //start merge
        
        let aVideoAsset : AVAsset = AVAsset(url: videoUrl as URL)
        let aAudioAsset : AVAsset = AVAsset(url: audioUrl as URL)
        
        mutableCompositionVideoTrack.append(mixComposition.addMutableTrack(withMediaType: AVMediaType.video, preferredTrackID: kCMPersistentTrackID_Invalid)!)
        mutableCompositionAudioTrack.append( mixComposition.addMutableTrack(withMediaType: AVMediaType.audio, preferredTrackID: kCMPersistentTrackID_Invalid)!)
        
        let aVideoAssetTrack : AVAssetTrack = aVideoAsset.tracks(withMediaType: AVMediaType.video)[0]
        let aAudioAssetTrack : AVAssetTrack = aAudioAsset.tracks(withMediaType: AVMediaType.audio)[0]
        
        
        
        do{
            try mutableCompositionVideoTrack[0].insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: aVideoAssetTrack.timeRange.duration), of: aVideoAssetTrack, at: CMTime.zero)
            
            //In my case my audio file is longer then video file so i took videoAsset duration
            //instead of audioAsset duration
            
            try mutableCompositionAudioTrack[0].insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: aVideoAssetTrack.timeRange.duration), of: aAudioAssetTrack, at: CMTime.zero)
            
            //Use this instead above line if your audiofile and video file's playing durations are same
            
            //            try mutableCompositionAudioTrack[0].insertTimeRange(CMTimeRangeMake(kCMTimeZero, aVideoAssetTrack.timeRange.duration), ofTrack: aAudioAssetTrack, atTime: kCMTimeZero)
            
        }catch{
            
        }
        
        totalVideoCompositionInstruction.timeRange = CMTimeRangeMake(start: CMTime.zero,duration: aVideoAssetTrack.timeRange.duration )
        
        let mutableVideoComposition : AVMutableVideoComposition = AVMutableVideoComposition()
        mutableVideoComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)
        
        mutableVideoComposition.renderSize = CGSize(width: 1280, height: 720)
        
//                playerItem = AVPlayerItem(asset: mixComposition)
//                player = AVPlayer(playerItem: playerItem!)
//
//
//                AVPlayerVC.player = player
        

        let savePathUrl = NSURL(fileURLWithPath: NSHomeDirectory() + "/Documents/newVideo.mp4")
        
        let assetExport: AVAssetExportSession = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPresetHighestQuality)!
        assetExport.outputFileType = AVFileType.mp4
        assetExport.outputURL = savePathUrl as URL
        assetExport.shouldOptimizeForNetworkUse = true

        assetExport.exportAsynchronously { () -> Void in
            switch assetExport.status {

            case AVAssetExportSessionStatus.completed:

                //Uncomment this if u want to store your video in asset

                let assetsLib = ALAssetsLibrary()
                assetsLib.writeVideoAtPath(toSavedPhotosAlbum: savePathUrl as URL, completionBlock: nil)

                print("success")
            case  AVAssetExportSessionStatus.failed:
                print("failed \(assetExport.error)")
            case AVAssetExportSessionStatus.cancelled:
                print("cancelled \(assetExport.error)")
            default:
                print("complete")
            }
        }
        
        return savePathUrl
        
    }

}
