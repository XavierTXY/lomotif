//
//  VideoView.swift
//  iOSTechnicalTest
//
//  Created by XavierTanXY on 24/9/19.
//  Copyright © 2019 Lomotif. All rights reserved.
//

import Foundation
import UIKit
import AVKit
import AVFoundation

class VideoView: UIView {
    
    var playerLayer: AVPlayerLayer?
    var player: AVPlayer?
    var isLoop: Bool = false
    var previewVC: VideoPreviewViewController!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    func configure(url: String) {
        if let videoURL = URL(string: url) {
            player = AVPlayer(url: videoURL)
            playerLayer = AVPlayerLayer(player: player)
            playerLayer?.frame = bounds
            playerLayer?.videoGravity = AVLayerVideoGravity.resize
            if let playerLayer = self.playerLayer {
                layer.addSublayer(playerLayer)
            }
            NotificationCenter.default.addObserver(self, selector: #selector(reachTheEndOfTheVideo(_:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.player?.currentItem)
        }
    }
    
    func play() {
        if player?.timeControlStatus != AVPlayer.TimeControlStatus.playing {
            player?.play()
        }
    }
    
    func pause() {
        player?.pause()
    }
    
    func stop() {
        player?.pause()
        player?.seek(to: CMTime.zero)
    }
    
    @objc func reachTheEndOfTheVideo(_ notification: Notification) {
        player?.pause()
        player?.seek(to: CMTime.zero)
        self.previewVC.vidReachEnd()
    }
    
    func getVidDuration(vidUrl: NSURL) -> Float {
        let asset = AVURLAsset(url:vidUrl as URL, options:nil)
        let duration: CMTime = asset.duration
        return Float(CMTimeGetSeconds(duration))
    }
}
