//
//  PictureCell.swift
//  iOSTechnicalTest
//
//  Created by XavierTanXY on 19/9/19.
//  Copyright © 2019 Lomotif. All rights reserved.
//

import UIKit

class PictureCell: UICollectionViewCell {
    
    @IBOutlet weak var picImgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(data: PixabayData) {
        
        picImgView.loadImageUsingCacheWithUrlString(url: data.imgUrl)
    }
    
}
